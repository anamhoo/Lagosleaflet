Una de las dificultades que hemos encontrado al hacer mapas con Leaflet es el añadir archivos geojson que se encuentren de forma local y asignar estilos y añadirlos al boton de control de capas, compartimos este ejercicio esperando pueda ser útil.

El ejercicio muestra un mapa con los polígonos de lo que antes eran los grandes lagos del Valle de México, ahora casi muertos con el crecimiento urbano.

Los polígonos fueron trazados a partir de mapas históricos disponibles en la mapoteca de Sagarpa. Adicionalmente existe la capa del polígono de obras de un aeropuerto internacional que se pretendía construir sobre la zona de uno de los grandes lagos. La información del polígono se obtuvo de la manifestación de impacto ambiental del proyecto.

Actualmente existen los archivos:

 - Lagos.html en el que se encuentra cómo añadir capas gejson externas

 - Lagos_addcontrol.html en la que se puede ver el código que nos permite añadir un botón de control para la capa de lagos.

 - Lagos_multilayer.html para añadir múltiples capas y que puedan ser escondidas a partir de un control.

En la carpeta /geojson_files se pueden encontrar los archivos geojson para los Lagos y para el polígono del aeropuerto.


Nota: Para poder visualizar las capas geojson sin que estén en un servidor necesitas correr un servicio web local.

Una forma muy sencilla es que uses:

 $ python3 -m http.server

o 

 $ python -m http.server

Después desde un navegador puedes copiar en la barra de direcciones el archivo html que quieras visualizar, por ejemplo:

http://localhost:8000/LagosLeaflet/Lagos.html
